export let renderFoodList = (foodArr) =>{
    let contentHTML = ""
    foodArr.forEach((item) => {
        let {
            maMon,
            tenMon,
            loai,
            giaMon,
            khuyenMai,
            tinhTrang
         } = item;
        console.log('item: ', item);
        let contentTr = `
        <tr>
           <td>${maMon}</td>
           <td>${tenMon}</td>
           <td>${loai ? "Chay" : "Mặn"}</td>
           <td>${giaMon}</td>
           <td>${khuyenMai}</td>
           <td>${item.tinhGiaKM()}</td>
           <td>${tinhTrang}</td>
           <td>
           <button onclick="xoaMonAn(${maMon})" class="btn btn-primary">Xóa</button>
            <button onclick="suaMon(${maMon})" class=" btn btn-danger">Sửa</button>
            </td>
           
        </tr>`;
        contentHTML += contentTr;
    });

    document.getElementById("tbodyFood").innerHTML = contentHTML;
}
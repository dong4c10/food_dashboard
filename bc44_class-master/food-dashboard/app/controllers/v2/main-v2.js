import { renderFoodList } from "./controller-v2.js";
import { Food } from "../../models/v1/model.js"
import { layThongTinTuForm } from "../v1/controller-v1.js";

const BASE_URL = "https://643bb2c744779455735ec355.mockapi.io/sv/id/food";

let fetchFoodList = () => {
  axios({
    url: BASE_URL,
    method: "GET",
  })
    .then((res) => {
      let foodArr = res.data.map((item) => {
        let { name, type, discount, img, desc, price, status, id } = item;

        let food = new Food(
            id, name, type, price, discount, status, img, desc );
        return food;
      });

      renderFoodList(foodArr);
      // return {
      //     name,
      //     type,
      //     discount,
      //     img,
      //     desc,
      //     price,
      //     status,
      //     id,
      // }
      console.log(res.data);
    })

    .catch((err) => {
      console.log(err);
    });
};

let xoaMonAn = (id) =>{
    axios({
      url: `${BASE_URL}/${id}`,
      method: "DELETE",
    })
    .then((res) => {
        fetchFoodList();
        console.log(res.data);
    })
    .catch((err) =>{
        console.log(err);
    })
   
}
window.themMon = () =>{
    let data = layThongTinTuForm()
    console.log('data: ', data);
    let newFood = {
        name: data.maMon,
        type: data.loai,
        discount: data.khuyenMai,
        img: data.hinhMon,
        desc: data.moTa,
        price: data.giaMon,
        status: data.tinhTrang,
    };
    axios({
        url: BASE_URL,
        method: "POST",
        data: newFood,
    })
    .then((res) => {
        $("#exampleModal").modal("hide");
        console.log(res.data);
        fetchFoodList();
    })
    .catch((err) => {
        console.log(err);
    })

};

window.suaMon = (id) => {
    
    
    axios({
        url: `${BASE_URL}/${id}`,
        method: "GET",
        
    })
    .then((res) => {
        $("#exampleModal").modal("show");
        console.log(res.data);
        let { id, name, type, price, discount, status, img, desc } = res.data;
        document.getElementById("foodID").value = id
        document.getElementById("tenMon").value = name
        document.getElementById("loai").value = type ? "loai1" : "loai2";
        document.getElementById("giaMon").value = price
        document.getElementById("khuyenMai").value = discount
        document.getElementById("tinhTrang").value = status ? "1" : "0";
        document.getElementById("hinhMon").value = img
        document.getElementById("moTa").value = desc

    })
    .catch((err) => {
        console.log(err);
    })
}
window.xoaMonAn = xoaMonAn;
fetchFoodList();
